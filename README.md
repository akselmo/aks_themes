# NOTE FOR COLORSCHEMES

For colorschemes, please check the Revontuli repository!
Colorschemes here wont be updated anymore!

Revontuli can be found here -> [Revontuli colorschemes](https://codeberg.org/akselmo/Revontuli)

# aks-themes
my kvantum, plasma and aurorae themes

the readme files should have the respective licenses and original creators mentioned

Default Breeze theme
[![Default breeze theme](./default_breeze.png)
Without accent color
[![Without accent color](./no_accent.png)](./no_accent.png)
With accent color
[![With accent color](./with_accent.png)](./with_accent.png)

The screenshot has tinting from accent color enabled. For konsole you have to
change the bg color manually as well.

# Fonts
UI: IBM Plex Sans, https://www.ibm.com/plex/
Code/Term/Mono/etc: Ligalex Mono https://github.com/ToxicFrog/Ligaturizer 
# aks-dark color scheme
[Themer link](https://themer.dev/?colors.dark.accent0=%23ff3344&colors.dark.accent1=%2333ffa0&colors.dark.accent2=%23ff9233&colors.dark.accent3=%235fff33&colors.dark.accent4=%2333ffff&colors.dark.accent5=%2333aaff&colors.dark.accent6=%23838fff&colors.dark.accent7=%23D970FF&colors.dark.shade0=%23202020&colors.dark.shade1=%23474247&colors.dark.shade2=%23656066&colors.dark.shade3=%23847E85&colors.dark.shade4=%23A29DA3&colors.dark.shade5=%23C1BCC2&colors.dark.shade6=%23E0DCE0&colors.dark.shade7=%23FFFFFF&colors.light.accent0=%23F03E4D&colors.light.accent1=%23F37735&colors.light.accent2=%23EEBA21&colors.light.accent3=%2397BD2D&colors.light.accent4=%231FC598&colors.light.accent5=%2353A6E1&colors.light.accent6=%23BF65F0&colors.light.accent7=%23EE4EB8&colors.light.shade0=%23FFFCFF&colors.light.shade1=%23E0DCE0&colors.light.shade2=%23C1BCC2&colors.light.shade3=%23A29DA3&colors.light.shade4=%23847E85&colors.light.shade5=%23656066&colors.light.shade6=%23474247&colors.light.shade7=%23282629&activeColorSet=dark&calculateIntermediaryShades.dark=true&calculateIntermediaryShades.light=true)

# oneaks theme

Much more vivid version of OneDark theme

Your theme's unique URL:

[Themer link](https://themer.dev/?colors.dark.accent0=%23ff3344&colors.dark.accent1=%2333ffa0&colors.dark.accent2=%23ff9233&colors.dark.accent3=%235fff33&colors.dark.accent4=%2333ffff&colors.dark.accent5=%2333aaff&colors.dark.accent6=%23838fff&colors.dark.accent7=%239966ff&colors.dark.shade0=%231E2227&colors.dark.shade1=%2323272E&colors.dark.shade2=%235C6370&colors.dark.shade3=%237F848E&colors.dark.shade4=%23ABB2BF&colors.dark.shade5=%23B2B9C7&colors.dark.shade6=%23E0DCE0&colors.dark.shade7=%23FFFFFF&colors.light.accent0=%23F03E4D&colors.light.accent1=%23F37735&colors.light.accent2=%23EEBA21&colors.light.accent3=%2397BD2D&colors.light.accent4=%231FC598&colors.light.accent5=%2353A6E1&colors.light.accent6=%23BF65F0&colors.light.accent7=%23EE4EB8&colors.light.shade0=%23FFFCFF&colors.light.shade1=%23E0DCE0&colors.light.shade2=%23C1BCC2&colors.light.shade3=%23A29DA3&colors.light.shade4=%23847E85&colors.light.shade5=%23656066&colors.light.shade6=%23474247&colors.light.shade7=%23282629&activeColorSet=dark&calculateIntermediaryShades.dark=false&calculateIntermediaryShades.light=true)

## Solarized edition

How to make plasma look "solarized" with aksdark theme: enable window tinting (0.20 in colors file) and set accent color to `#37cbe9`

# aks-dark_round
This theme uses some basic breeze files and Arc-Dark files

# kvantum/Aks-dark
based on Qogir https://github.com/vinceliuice/Qogir-kde

# kvantum/Aks-dark_round
based on arc-dark kvantum

# classik/klassy
with round edges use classik for window decoration and and style https://github.com/paulmcauley/classik

you can also use lightlyshaders to get rounded edges to ALL window decorations https://github.com/a-parhom/LightlyShaders

# Breezur

Use with https://github.com/a-parhom/RoundedSBE
- Set outline strength to 10%

Also use Klassy theme with my modifications:
https://github.com/Akselmo/klassy/tree/use-buttoncolor-for-rightclick-outline