#! bash oh-my-bash.module

# Aks is a simple one-liner theme built on top of rr theme: https://github.com/ohmybash/oh-my-bash/blob/master/themes/rr/rr.theme.sh
# Basically just modified rr to fit my needs
#

OMB_PROMPT_SHOW_PYTHON_VENV=${OMB_PROMPT_SHOW_PYTHON_VENV:=true}
OMB_PROMPT_VIRTUALENV_FORMAT="${_omb_prompt_bold_green} ❲p:${_omb_prompt_reset_color}%s${_omb_prompt_bold_green}❳${_omb_prompt_reset_color}"
OMB_PROMPT_CONDAENV_FORMAT="${_omb_prompt_bold_purple} ❲c:${_omb_prompt_reset_color}%s${_omb_prompt_bold_purple}❳${_omb_prompt_reset_color}"

function _omb_theme_PROMPT_COMMAND() {
  local input_start="${_omb_prompt_bold_green}>${_omb_prompt_reset_color}"
  local user_name="${_omb_prompt_white}\u${_omb_prompt_reset_color}"
  local base_directory="${_omb_prompt_bold_blue}❲\w❳${_omb_prompt_reset_color}"
  local GIT_THEME_PROMPT_PREFIX="${_omb_prompt_bold_cyan} ❲:${_omb_prompt_reset_color}"
  local SVN_THEME_PROMPT_PREFIX="${_omb_prompt_bold_cyan} ❲:${_omb_prompt_reset_color}"
  local HG_THEME_PROMPT_PREFIX="${_omb_prompt_bold_cyan} ❲:${_omb_prompt_reset_color}"
  local SCM_THEME_PROMPT_SUFFIX="${_omb_prompt_bold_cyan}❳${_omb_prompt_reset_color}"
  local SCM_THEME_PROMPT_CLEAN="${_omb_prompt_bold_green} ✔${_omb_prompt_reset_color}"
  local SCM_THEME_PROMPT_DIRTY="${_omb_prompt_bold_red} ⨯${_omb_prompt_reset_color}"

  PS1="${user_name} ${base_directory}"

  local python_venv
  _omb_prompt_get_python_venv
  PS1+=$python_venv

  local scm_info=$(scm_prompt_info)
  PS1+=${scm_info:+$scm_info }
  PS1+=$_omb_prompt_normal
  PS1+="\n${input_start} "
}

_omb_util_add_prompt_command _omb_theme_PROMPT_COMMAND
